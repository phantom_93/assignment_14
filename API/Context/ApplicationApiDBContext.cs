﻿using Assignment14.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment14.Context
{
    public class ApplicationApiDBContext : DbContext
    {
        public DbSet<Franchise> Franchises { get; set; }

        public ApplicationApiDBContext(DbContextOptions<ApplicationApiDBContext> contextOptions)
            : base(contextOptions)
        {
            Database.EnsureCreated();
            //Database.Migrate();
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // seed franchise Data
            modelBuilder.Entity<Franchise>().HasData(
                  new Franchise()
                  {
                      Id = 1,
                      Name = "DC Universe",
                      Description = "The best cinematic Universe"
                  }
                  );
            modelBuilder.Entity<Franchise>().HasData(
             new Franchise()
             {
                 Id = 2,
                 Name = "Marvel Cinematic Universe",
                 Description = "The second to None Universe"
             }
             );

        }
       
    }
}
