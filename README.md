**Assignment 14 - Docker**

Assignment 14: Docker and .NET

You are to create a simple .NET Core Web API with EF. This application can be as simple as one entity and crud - as long as there is information coming through.

Docker requirements:

Create a Dockerfile to replicate your API
Use docker-compose to have a container of SQL Server in the container.
Build and run it locally to ensure it is working as intended. 
Place the image on dockerhub.
---------------------------------------------------------------------------------------------------------------------
A simple movie franchise API is dockerized and the details of docker configuration can be found in the docker-compose.yml where a bridge network is applied. 

"In terms of Docker, a bridge network uses a software bridge which allows containers connected to the same bridge network to communicate, while providing isolation from containers which are not connected to that bridge network. The Docker bridge driver automatically installs rules in the host machine so that containers on different bridge networks cannot communicate directly with each other." (quoted from https://docs.docker.com/network/bridge/#:~:text=In%20terms%20of%20Docker%2C%20a,connected%20to%20that%20bridge%20network.)

run assignment_14 docker image where both API docker image and db docker image has to run in parallel at the same time.
API Endpoint for franchises: http://localhost:5000/api/franchises
Use Postman tool to do the CRUD (Create, Read, Update and Delete) operations

The following commands can be used for different purposes 

POST: to add/ create a franchise without using id in json format as it will automatically generate id. 

GET: - retrieve/read all franchises

Get(id) - retrieve/read a specific franchise by its id for example: (http://localhost:5000/api/franchises/2)

PUT: to update a franchise  for example (http://localhost:5000/api/franchises/2 and use id in json format)

DELETE: to delete a franchise for example (http://localhost:5000/api/franchises/2  and Send)




Docker Image of API: https://hub.docker.com/r/phantom31/docker_assignment14_api

Docker Image of Database: https://hub.docker.com/r/phantom31/docker_assignment14_db 